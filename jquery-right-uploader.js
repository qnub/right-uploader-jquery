/***
 Right Uploader - jQuery ajax file upload plugin.

 Author: qnub (http://qnub.ru)

 License: LGPL
*/
(function( $ ) {
    $.fn.right_uploader = function(o) {

        var settings = $.extend({
            url: '/ajax/fileupload',
            field_name: 'file',
            max_files: 5,
            allowed_files: [],
            display_image: false,
            delete_file: true,
            files: {},
            debug: false,
            file_field_desc: 'Maximum size 16MiB.',
            error_load_msg: 'Uploading error. Please, try again.',
            error_format_msg: 'Deprecated file format. Allowed only:',
            delete_hint: 'Delete file',
            csrf_token:'',
            csrf_name: 'csrfmiddlewaretoken',
            success_callback: function(){}
        }, o || {});

        var container = this;
        var uploaded = [];
        var uploaded_cnt = 0;
        var upload_field_show = false;

        if (settings['file_field_desc']){
            settings['file_field_desc'] = $('<span></span>').addClass('right_uploader_field_description').
                text(settings['file_field_desc']);
        }

        if (settings['error_format_msg']){
            settings['error_format_msg'] = settings['error_format_msg'] + ' ' + settings['allowed_files'].join(', ');
        }

        var hidden_area = $('<div></div>').addClass('right_uploader_hidden_area');
        var uploaded_area = $('<div></div>').addClass('right_uploader_uploaded_area');
        var add_file_area = $('<div></div>').addClass('right_uploader_add_file_area');

        container.append(hidden_area).
            append(uploaded_area).
            append(add_file_area);

        add_new_uploaded_file(settings['files']);

        function add_new_uploaded_file(data){
            var data = data || {};

            for (var file_id in data) {
                $(hidden_area).append($('<input/>').attr('name',settings['field_name']).
                    attr('type','hidden').attr('id','rupldrflID__'+file_id).val(file_id));
                uploaded[file_id] = data[file_id];
                uploaded_cnt++;

                var file = $('<div></div>');
                if (settings['display_image']){
                    $(file).html($('<img/>').attr('src',data[file_id]).
                        addClass('right_uploader_image').hide().load(function(){$(this).fadeIn(750)}));
                }else{
                    $(file).text(data[file_id]);
                }

                var file_show = $('<div></div>').addClass('right_uploader_file_uploaded').
                    attr('id','rupldrswID__'+file_id).html(file).hide().toggle(1000);

                if (settings['delete_file']){
                    $(file_show).append($('<div></div>').addClass('right_uploader_delete_btn').
                        attr('id','rupldrID__'+file_id).attr('title', settings['delete_hint']).
                        click(on_file_delete));
                }

                $(uploaded_area).append(file_show);
            }

            if (uploaded_cnt < settings['max_files'] && !upload_field_show){
                $(add_file_area).append($('<input/>').attr('name',settings['field_name']).
                    attr('type','file').addClass('right_uploader_file_field').change(on_file_field_change)).
                    append(settings['file_field_desc']);
                upload_field_show = true;
            }
        }

        function on_file_field_change(){
            var $this = $(this);
            var filename = $this.val().substr($this.val().lastIndexOf('\\')+1);

            if (!settings['allowed_files'] ||
                $.inArray(filename.substr(filename.lastIndexOf('.')+1).toLowerCase(),settings['allowed_files']) != -1){
                var iid = 'rupldifrm__' + Math.floor(Math.random() * 99999);

                var send_form = $('<form></form>').
                    css('position','absolute').
                    css('top','-9000px').
                    css('right','-9000px').
                    attr('action',settings['url']).
                    attr('method','post').
                    attr('target',iid).
                    attr('id','rupldfrm__'+iid).
                    attr('enctype','multipart/form-data').
                    append(this).
                    append($('<input/>').
                    attr('name',settings['csrf_name']).
                    val(settings['csrf_token'])).hide();

                var iframe = $('<iframe></iframe>').
                    css('position','absolute').
                    css('top','-9000px').
                    css('right','-9000px').
                    attr('src',settings['url']).
                    attr('id',iid).
                    attr('name',iid).
                    load(on_iframe_load).hide();

                $('body').append(iframe).append(send_form);
                send_form.submit();
                $this.remove();
                upload_field_show = false;
                $(add_file_area).html('<div class="right_uploader_loading"></div>');
            }else{
                $this.val('').attr('file','');
                alert(settings['error_format_msg']);
            }
        }

        function on_iframe_load(){
            var raw = $(this).contents().find('body').text();
            var answer = $.parseJSON(raw);
            $('#rupldfrm__'+$(this).attr('id')).remove();
            $(this).remove();

            if (answer['success']){
                var data = {};
                if (settings['display_image']){
                    data[answer['id']] = answer['link'];
                }else{
                    data[answer['id']] = answer['filename'];
                }
                on_file_uploaded(data);
            }else{
                on_upload_failed(answer['error']);
            }
        }

        function on_file_uploaded(data){
            $(add_file_area).html('');
            upload_field_show = false;
            add_new_uploaded_file(data);
            settings['success_callback']();
        }

        function on_upload_failed(msg){
            if (settings['debug']){
                msg = settings['error_load_msg']+'<br/>'+msg;
            }else{
                msg = settings['error_load_msg'];
            }

            $(add_file_area).html($('<div></div>').addClass('right_uploader_error').
                html(msg));
            add_new_uploaded_file();
        }

        function on_file_delete(){
            var id = $(this).attr('id');
            id = id.substr(id.lastIndexOf('__')+2);
            $.getJSON(settings['url']+'?delete='+id, on_file_deleted);
        }

        function on_file_deleted(data){
            if (data['success']){
                $('#rupldrflID__'+data['id']).remove();
                $('#rupldrswID__'+data['id']).fadeOut(500, function() { $('#rupldrswID__'+data['id']).remove(); });
                uploaded_cnt--;
                add_new_uploaded_file();
            }
        }
    };
})( jQuery );
