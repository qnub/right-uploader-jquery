About
=====

**jQuery** plugin for AJAX (ifarme) file uploading.

:Author: qnub (http://qnub.ru)

:License: LGPL

Example
=======

::

    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="js/jquery-right-uploader.js"></script>
    <script type="text/javascript">
    $(document).ready(*function*(){
        $('#upload-section').right_uploader({
            url: '/ajax/upload',
            field_name:'photos',
            allowed_files:['jpg','jpeg','png'],
            display_image: true,
            delete_file: true,
            delete_hint: 'Удалить фото',
            files: {1:'http://external.example.com/uploaded/first.jpg', 2:'/img/another.jpg'},
            debug: false,
            max_files: 5,
            csrf_token: 'ksdz8er7owinvgjr985jkanje93po',
            csrf_name: 'csrfmiddlewaretoken',
            file_field_desc: 'Максимальный размер 16МБ.',
            error_load_msg: 'Ошибка загрузки. Повторите попытку.',
            error_format_msg: 'Запрещённый формат. Разрешены только:'
        });
    });
    </script>
    
English
=======
    
Full options list
-----------------

:url: *string* **URL** for file uploading, add ``?filename=filename.ext`` while upload;
:field_name: *string* <input/> field name, value of this field set to returned ID of uploaded file;
:max_files: *integer* maximum number of files;
:allowed_files: *array* of allowed file extensions, not check if empty;
:display_image: *boolean* flag, display uploaded files or not, if ``false`` - show filenames;
:delete_file: *boolean* flag accept deletions of uploaded files or not, if ``true`` - 
    display clickable region after filename/thumbnail. Add ``?delete=file_id`` to ``url`` 
    while make ``$.getJSON`` request;
:files: *object* with already uploaded files (for subsequent edit) in format: ``{fileID:'filename|fileurl'}``;
:debug: *boolean* flag for show or not returned error *string*;
:file_field_desc: *string* description for file selection field;
:error_load_msg: *string* for upload error message;
:error_format_msg: *string* for alert about deprecated file format, after *string* added 
    allowed format list from ``allowed_files``;
:delete_hint: *string* hint for file deletion region;
:csrf_token: *string* secret token for CSRF (for **Django** or same system);
:csrf_name: *string* for <input/> field name with ``csrf_token`` value;
:success_callback: *function* for callback on succes upload.

Answer format
-------------

JSON with format:

:success: *boolean* flag of uploading result;
:error: *string* error *string*, if ``success`` is ``false`` end ``debug`` option is ``true``;
:id: ID of uploaded file;
:filename: *string* filename/url of uploaded file (URL if ``display_image`` is ``true`` for 
    display uploaded file thumbnail);
    
Русский
=======
    
Полный список опций
-------------------

:url: *string* **URL** для загрузки файлов, во время загрузки 
    к нему добавляется ``?filename=filename.ext``;
:field_name: *string* имя поля <input/>, значение поля устанавливается равным ID загруженного файла;
:max_files: *integer* максимальное количество загружаемых файлов;
:allowed_files: *array* список разрешённых к загрузке расширений;
:display_image: *boolean* флаг, отображать или нет миниатюру загруженных файлов, если ``false`` - 
    будут показывать имена файлов;
:delete_file: *boolean* флаг позволяющий удалять загруженные файлы, если ``true`` - 
    отображает кликабельную область сразу после имени файла/миниатюры. Добавляет ``?delete=file_id`` 
    к ``url`` во вермя выполенния ``$.getJSON`` запроса;
:files: объект содержащий список уже загруженных файлов (для последующего редактирования) 
    в формате: ``{fileID:'filename|fileurl'}``;
:debug: *boolean* флаг отображать или нет фозвращаемую строку описания ошибки;
:file_field_desc: *string* описание поля выбора файлов;
:error_load_msg: *string* текст отображаемы при ошибке загрузки;
:error_format_msg: *string* текст сообщающей о выборе файлов недопустимого 
    формата, после него добавляется список
    разрешённых форматов из ``allowed_files``;
:delete_hint: *string* подсказка к области удаления файла;
:csrf_token: *string* секретный ключ CSRF (для **Django** или аналогичных систем);
:csrf_name: *string* имя поля <input/> со значением ключа ``csrf_token``;
:success_callback: *function* функция, вызываемая в случае удачной загрузки.

Формат ответа
-------------

JSON следующего формата:

:success: *boolean* флаг успешности загрузки;
:error: *string* описание ошибки, на случай если ``success`` равен ``false`` 
    и опция ``debug`` равна ``true``;
:id: ID загруженного файла;
:filename: *string* filename/url загруженного файла (URL если опция ``display_image`` равна ``true`` для
    отображения миниатюры загруженного файла);
